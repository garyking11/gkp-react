import React from 'react';
 require('../assets/images/colors.jpg');
class CoverArt extends React.Component {
    constructor(props){
        super(props);

        console.log(this.props)
    }


    render() {

        return (
            <div>
                <h1>CoverArt</h1>
                <p>{this.props.artwork}</p>
                <img src={`../assets/images/${this.props.artwork}`} alt={this.props.artwork}/>
                <p>{this.props.description}</p>
            </div>
        )
    }

}

export default CoverArt;