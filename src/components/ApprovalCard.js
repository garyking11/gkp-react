import React from 'react';
import '../assets/css/styles.scss';
import CoverArt from "./CoverArt";
class ApprovalCard extends React.Component {
    render() {
        return (
            <div
                 style={{margin: '10px 0', backgroundColor: '#d8d8d8', borderBottom: '1px solid #333', padding: '20px'}}>

                <h2>title</h2>
                <div className="content">children</div>
                <div className="extra content">

             {/*       <h2>{props.songTitle}</h2>
                    <div className="content">{props.children}</div>*/}


                    <div className="ui two buttons">
                        <div className="ui basic green button">Approve</div>
                        <div className="ui basic red button">Reject</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ApprovalCard;