import React from 'react';

const Details = props => {

    return (
        <div>

            <hr />
            <div className={'container row'}>
                <hr/>
                <div className={'col-sm-12 col-md-3 col-lg-3'}>
                    <img src={require('../assets/images/colors.jpg')} alt={props.artwork}/>
                </div>

                <div className={'col-sm-12 col-md-9 col-lg-9'}>
                    <h2>{props.title}</h2>
                    {<h5>Please <a href="#" onClick={props.handleLogin}>Login </a> to collaborate!</h5>}
                    <p>{props.isLoggedIn}</p>

                    <p>{props.name}</p>
                    <p>{props.linkToWebsite}</p>
                    <p>{props.linkToWebsiteText}</p>
                    <p>{props.description}</p>
                    <p>{props.localLink}</p>
                    <p>{props.localLinkText}</p>

                </div>
            </div>
        </div>
    )
}

export default Details;