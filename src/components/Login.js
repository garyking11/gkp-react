import React from 'react';
import {Form} from "react-bootstrap";
import Content from './Content';
import '../assets/css/styles.scss';

class Login extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (

            <div className="wrapperStyle">


                <h2>Login</h2>
                <h4>{this.props.loginText}</h4>

                <div className="show" role="alert">

                    <Form>
                        <Form.Group controlId="formBasicEmail3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password"/>
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox3">
                            <Form.Check type="checkbox" label="Remember Me"/>
                        </Form.Group>


                    </Form>

                    <h3>{'Need a list of songs!'}</h3>
                    {/*<p>{this.state.isLoaded}</p>*/}
                    {/*<Admin accessAllowed={this.state.accessAllowed}/>*/}
                    <Content copyright={this.props.copyright}/>

                </div>
            </div>
        )
    }
}

export default Login;

