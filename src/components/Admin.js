import React from 'react';
import {Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import Button from "react-bootstrap/Button";

class Admin extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={'borderBottom'} style={{padding: '20px'}}>

                <h2>Admin</h2>
                <div className="show" role="alert">
                    <strong>Oh snap! You got an error!</strong>
                    <p>
                        Change this and that and try again.
                    </p>
                    <Form>
                        <Form.Group controlId="formBasicEmail2">
                            <Form.Label>Song Title</Form.Label>
                            <Form.Control type="text" placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                Song Title
                            </Form.Text>
                        </Form.Group>


                        <Form.Group controlId="formBasicCheckbox2">
                            <Form.Check type="checkbox" label="I am finished"/>
                        </Form.Group>


                    </Form>
                </div>
            </div>
        )
    }
}

export default Admin;