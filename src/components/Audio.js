import React from 'react';

const Audio = props => {

    console.log(props);

    return (
        <div>
            <div key={props.id}>{props.children}</div>
            <h1>Audio</h1>
            <hr/>
            <h2>{props.title}</h2>
            <p>{props.audiofile}</p>

            <hr/>
            <p>{props.name}</p>
            <p>{props.linkToWebsite}</p>
            <p>{props.linkToWebsiteText}</p>
            <p>{props.description}</p>
            <p>{props.localLink}</p>
            <p>{props.localLinkText}</p>

            <hr/>

            {<h3>Please <a href="#" onClick={props.handleLogin}>Login </a> to collaborate!</h3>}
            <p>{props.isLoggedIn}</p>
        </div>
    )

}

export default Audio;