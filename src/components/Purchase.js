import React from 'react';

class Purchase extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="purchase">Purchase button: {this.props.purchase}</div>)
    }
}

export default Purchase;