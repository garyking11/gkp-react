import React from 'react';
import {Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import '../assets/css/styles.scss';

class SubmitForm extends React.Component {
    render() {
        return (
            <div>

                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                    <h2>Submit</h2>
                    <strong>Oh snap! You got an error!</strong>
                    <p>
                        Change this and that and try again.
                    </p>
                    <Form>
                        <Form.Group controlId="1">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password"/>
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox1">
                            <Form.Check type="checkbox" label="Check me out"/>
                        </Form.Group>
                        <Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form.Group>

                    </Form>
                </div>

            </div>
        )
    }
}

export default SubmitForm;