import React from 'react';

class Photos extends React.Component{

    constructor(props){
        super(props)
    }

    render() {
        return (
            <div>
                <div className="photos">  {<img alt={this.props.cover} src={require('../assets/images/album/'+this.props.photos)}/>}

                </div>
            </div>
        )
    }
}
export default Photos;
