import React from 'react';
import {Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
class Upload extends React.Component {

    render() {
        return (
            <div className={'upload'}>


                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                    <h3>Upload</h3>
                    <strong>Oh snap! You got an error!</strong>
                    <p>
                        Change this and that and try again.
                    </p>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password"/>
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out"/>
                        </Form.Group>
                        <Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form.Group>

                    </Form>
                </div>


            </div>
        )
    }
}

export default Upload;

