import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/styles.scss';
import axios from 'axios';
import Details from './components/Details';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
class Project extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [{
                title: 'THE OLD TITLE',
                audiofile: '',
                author: '',
                isLoggedIn: true,
                artwork: '',
                name: '',
                linkToWebsite: '',
                linkToWebsiteText: '',
                localLink: '',
                localLinkText: ''
            }]
        }
    };

    componentDidMount() {
        this.getProjectData();
        this.handleLogin();
        console.log('Original state: next line');
        //console.log(this.state);
    }

    handleLogin = () => {
        console.log('fired');
        this.setState({isLoggedIn: true});
    }

    async getProjectData() {
        const url = `../data/data.json`;
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        await axios.get(url, {
            params: {
                headers: headers
            }
        })
            .then(response => response.data)
            .then(response => {

                this.setState({
                    data: response.data[0].details,

                    isLoggedIn: true,
                });

                let projects = this.state;
                console.log(projects);
                //return projects;
            })
            .catch(function (error) {
                console.log(error);
            });

    } // end getProjectData()


    render() {

        let items = this.state.data;
        // console.log(items)
        return (

            <div className={'container'}>
                {
                    items.map((item, i) =>
                        <Details
                            title={item.title}
                            audiofile={item.audiofile}
                            name={item.name}
                            description={item.description}
                            linkToWebsite={item.linkToWebsite}
                            linkToWebsiteText={item.linkToWebsiteText}
                            localLink={item.localLink}
                            localLinkText={item.localLinkText}
                            artwork={item.artwork}>
                        </Details>
                    )
                }
            </div>
        )
    } // end render()
};

ReactDOM.render(
    <Project/>, document.querySelector('#root')
);


