<div class="container ">
    <div class="page-top ">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 center">
                <?php require_once 'assets/includes/logo.php'; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12" style="position: relative;">
                <?php require_once 'assets/includes/nav.php'; ?>
            </div>
        </div>
    </div>
</div>